import Vue  from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {
    todoList: [
      {text:'qwe',id:1,priority:0,isDone:false},
      {text:'asd',id:2,priority:3,isDone:false},
      {text:'zxc',id:3,priority:2,isDone:false},
      {text:'rty',id:4,priority:2,isDone:false},
      {text:'fgh',id:5,priority:1,isDone:false},
      {text:'vbn',id:6,priority:2,isDone:false},
      {text:'uio',id:7,priority:1,isDone:false}
    ],
    newTask: {
      text: '',
      priority: 0,
      isDone: false,
      id: 0
    },
    priorities: [
      {value: 0, color: '#FFFFFF'},
      {value: 1, color: '#FAFF7D'},
      {value: 2, color: '#FFDB7D'},
      {value: 3, color: '#FE775C'},
    ]
  },
  getters: {
    getTodoItem: state => id => state.todoList.find(item => item.id === id)
  },
  mutations: {
    addTaskText(state, text) {
      state.newTask.text = text;
    },
    addTaskPriority(state, priority) {
      state.newTask.priority = priority;
    },
    addNewTask(state) {
      let taskToAdd = {
        text: state.newTask.text,
        id: state.newTask.id,
        priority: state.newTask.priority,
        isDone: state.newTask.isDone
      }
      state.todoList.push(taskToAdd);
    },
    setNewTaskId(state) {
      state.newTask.id = state.todoList.length;
    },
    resetNewTask(state) {
      state.newTask.text = '';
      state.newTask.priority = 0;
    },
    checkTask(state, payload) {
      const itemIndex = findItemIndex(payload, state.todoList);
      state.todoList[itemIndex].isDone = !state.todoList[itemIndex].isDone;
    },
    deleteTask(state, payload) {
      state.todoList.splice(findItemIndex(payload, state.todoList), 1);
    }
  }
});

const findItemIndex = (item, array) => array.indexOf(item);
